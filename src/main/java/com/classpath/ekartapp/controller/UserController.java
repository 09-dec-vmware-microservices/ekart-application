package com.classpath.ekartapp.controller;

import com.classpath.ekartapp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private OAuth2AuthorizedClientService authorizedClientService;


    @GetMapping
    public List<User> fetchAllUsers(OAuth2AuthenticationToken authenticationToken){
        System.out.println("AA" +authenticationToken.getPrincipal());
        System.out.println(authenticationToken.getName());
        System.out.println(authenticationToken.getCredentials());
        System.out.println(authenticationToken.isAuthenticated());
        OAuth2AuthorizedClient client = authorizedClientService
                .loadAuthorizedClient(authenticationToken.getAuthorizedClientRegistrationId(), authenticationToken.getName());
        System.out.println("Access token :: " +client.getAccessToken().getTokenValue());
        System.out.println("Access token :: " +client.getAccessToken().getTokenType());
        System.out.println("Access token :: " +client.getAccessToken().getScopes());
        System.out.println("Access token :: " +client.getAccessToken().getExpiresAt());
        System.out.println("Access token :: " +client.getAccessToken().getIssuedAt());
        String userInfoEndpointUri = client.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri();

        if (!StringUtils.isEmpty(userInfoEndpointUri)) {
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken().getTokenValue());
            HttpEntity entity = new HttpEntity("", headers);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<Map> response = restTemplate.exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
            Map userAttributes = response.getBody();

            Iterator it = userAttributes.keySet().iterator();
            while (it.hasNext()){
                final Object next = it.next();
                System.out.print("Key : " +next);
                System.out.println("    Value : " +userAttributes.get(next));
            }

        }


        return List.of(User.builder().username("Ramesh").emailAddress("ramesh@gmail.com").build(),
                User.builder().username("Vishnu").emailAddress("vishnu@gmail.com").build(),
                User.builder().username("Suresh").emailAddress("suresh@gmail.com").build(),
                User.builder().username("Rakesh").emailAddress("rakesh@gmail.com").build(),
                User.builder().username("Animesh").emailAddress("animiesh@gmail.com").build()
        );
    }
}